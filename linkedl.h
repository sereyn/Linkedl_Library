#ifndef LINKEDL
#define LINKEDL

#include <stdlib.h>
#include <stdio.h>

typedef struct Element{
	struct Element *next;
	int value;
} Element;

typedef struct Linkedl{
	Element *first;
	int length;
} Linkedl;

Linkedl createLl(void);

void pushLl(Linkedl*, int);

void printLl(const Linkedl);

void popLl(Linkedl*);

void freeLl(Linkedl* Ll);

int getLl(const Linkedl, const int);

void setLl(Linkedl, const int, const int);

#endif //LINKEDL