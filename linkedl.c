#include "linkedl.h"

Linkedl createLl(void){
	Linkedl Ll = {NULL, 0};
	return Ll;
}

void pushLl(Linkedl* Ll, int a){
	Element *e = malloc(sizeof(*e));
	e->next = NULL;
	e->value = a;
	if(Ll->length == 0){
		Ll->first = e;
	}else{
		Element *cur = Ll->first;
		int i = 0;
		for(; i < Ll->length-1; i++){
			cur = cur->next;
		}
		cur->next = e;
	}
	Ll->length++;
}

void printLl(const Linkedl Ll){
	if(Ll.length == 0){
		fprintf(stderr, "Cannot read an empty Linkedl.");
	}else{
		Element *cur = Ll.first;
		int i = 0;
		for(; i < Ll.length; i++){
			fprintf(stdout, "Linkedl[%d] = %d;\n", i, cur->value);
			cur = cur->next;
		}
	}
}

void popLl(Linkedl* Ll){
	if(Ll->length == 0){
		fprintf(stderr, "Cannot pop an empty Linkedl.");
	}else if(Ll->length == 1){
		free(Ll->first);
		Ll->first = NULL;
	}else{
		Element *cur = Ll->first;
		int i = 0;
		for(; i < Ll->length-1; i++){
			cur = cur->next;
		}
		cur->next = NULL;
		free(cur->next);
	}
	Ll->length--;
}

void freeLl(Linkedl* Ll){
	int i = 0;
	for(; i < Ll->length; i++)
		popLl(Ll);
}

int getLl(const Linkedl Ll, const int a){
	if(Ll.length == 0){
		fprintf(stderr, "Cannot get in an empty Linkedl.");
	}else{
		Element* cur = Ll.first;
		int i = 0;
		for(; i < a; i++){
			cur = cur->next;
		}
		return cur->value;
	}
}

void setLl(Linkedl Ll, const int a, const int b){
	if(Ll.length == 0){
		fprintf(stderr, "Cannot set in an empty Linkedl.");
	}else{
		Element* cur = Ll.first;
		int i = 0;
		for(; i < a; i++){
			cur = cur->next;
		}
		cur->value = b;
	}
}
