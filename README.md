# Linkedl_Library
A simple library to use linked lists

### Structures
##### Element
```c
typedef struct Element{
	struct Element *next;
	int value;
} Element;
```

This structure represent a linked list object.
It has a normal **value**, like an array, and a **pointer** to the
next element of the linked list

##### Linkedl
```c
typedef struct Linkedl{
	Element *first;
	int length;
} Linkedl;
```

This main structure represent our linked list.
It has a **pointer** to the **first element**, and a **length**.
It's composed of *Element*.

### Functions
##### createLl()
Return an empty Linkedl structure.<br>
Example: `Linkedl myList = createLl();`

##### pushLl(&myList, value)
Add at the end of the linked list an element<br>
with his value.<br>
Example: `pushLl(&myList, 5);`

##### popLl(&myList)
Free the last element of the linked list.<br>
Example: `popLl(&myList);`

##### freeLl(&myList)
Free the whole linked list.<br>
Example: `freeLl(&myList);`

##### getLl(myList, index)
Get the element's value at the indicated index.<br>
Example: `int myVariable = getLl(myList, 2);`

##### setLl(myList, index, value)
Set the element's value at the indicated index.<br>
Example: `setLl(myList, 2, 5);`

##### printLl(myList)
Print int *stdout* the indicated linked list<br>
Example: `printLl(myList);`
