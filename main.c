#include <stdio.h>
#include <stdlib.h>
#include <bool.h>

#include "linkedl.h"

int main(int argc, char *argv[]){
	Linkedl myList = createLl();

	pushLl(&myList, 5);
	pushLl(&myList, 14);
	pushLl(&myList, 2);
	pushLl(&myList, 40);

	setLl(myList, 2, 20);

	printLl(myList);
	freeLl(&myList);
	return 0;
}
